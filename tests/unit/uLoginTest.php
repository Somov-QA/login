<?php

include_once '../../validation.php';

class uLoginTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSomeFeature()
    {
        $v = new Validation();
        $result = $v->check('test', '123');

        $this->assertTrue($result);
    }
}