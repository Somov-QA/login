<?php
    $username = $_GET['username'];
    $password = $_GET['password'];
?>
<!doctype html>
<html>
<head>
    <title>Check Login</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
<body>
    <?php
        include_once 'validation.php';

        $v = new Validation();
        if($v->check($username, $password) == true) echo "<h2>Correct</h2>";
        else echo "<h2>Incorrect</h2>";
    ?>
</body>
</html>
